#20. Driver login to auto check in

**Url**

```sh
<basePath>/api/login/v3.json
```

```Note
External driver if while login is not checked in, then external driver will get auto checked in
```

**Method**

```sh
POST
```

**Headers**

```sh
Content-type: application/json
x-language-code: en
```

**Input Body**

```
No Changes
```

**Output Body**

```
No changes
```