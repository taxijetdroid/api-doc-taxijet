#24. Driver offline booking: signup passenger user

**Url**

```sh
<basePath>/api/driver-offline-booking/send-otp.json
```

```Note
This api will signup the passenger user via driver app.
```

**Method**

```sh
POST
```

**Headers**

```sh
Content-type: application/json
x-language-code: en
```

**Input Body**

```
Please add the following input body to the existing input body IN CASE OF PASSENGER REGISTRATION VIA DRIVER APP
```

```
{ 
   "driverOfflineBookingInputModel":{ 
      "otp":"1234",
      "driverOfflineCreatedPassenger":true,
      "driverId":"12sdasdasdasdsadas34"
   }
}
```

**Output Body**

```
{ 
   "userId":"c41ba410431d4a11bb5fc7c512f98d73"
}
```