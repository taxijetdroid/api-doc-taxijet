#22. Driver offline booking: validate existing user

**Url**

```sh
<basePath>/api/driver-offline-booking/validate-existing-user.json
```

```Note
This api will validate the existing user on the basis of phone number and position of characters in the password.
```

**Method**

```sh
POST
```

**Headers**

```sh
Content-type: application/json
x-language-code: en
```

**Input Body**

```
{ 
   "phoneNumber":"+2251234511111",
   "firstPasswordCharacterPosition":3,
   "secondPasswordCharacterPosition":6,
   "firstPasswordCharacter":"3",
   "secondPasswordCharacter":"6"
}
```

**Output Body**

```
{ 
   "userId":"c41ba410431d4a11bb5fc7c512f98d73"
}

{ 
   "type":"ERROR-BUSSINESS",
   "messages":[ 
      "Invalid login credentials."
   ]
}

{ 
   "type":"ERROR-BUSSINESS",
   "messages":[ 
      "No user found"
   ]
}
```