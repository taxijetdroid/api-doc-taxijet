#14. Estimated Fare Credits Used (Ride later, ride now, ride share)

**Url**

```sh
<basePath>/api/tours/estimate-fare.json

<basePath>/api/ride-share/estimate-fare.json
```

**Method**

```sh
GET
```

**Headers**

```sh
Content-type: application/json
x-language-code: en
```

```
NOTE: Please check the other parameters that are sent in the input body.
```

**Input Body**

```
No Changes
```

**Output Body**

```
//Ride share
{
   "chargesWithoutPromoCode":1000,
   "chargesWithPromoCode":1000,
   "oneSeatFareChargesWithPromoCode":1000,
   "twoSeatFareChargesWithPromoCode":1000,
   "threeSeatFareChargesWithPromoCode":1000,
   "oneSeatFareChargesWithoutPromoCode":1000,
   "twoSeatFareChargesWithoutPromoCode":1000,
   "threeSeatFareChargesWithoutPromoCode":1000,
    
   "estimatedTotalUserCredits": 2000
   "estimatedCreditsUsed":2000,
   "estimatedCashToBePaid":0,
   "estimatedNegativeCreditsAdjusted":1000,
   "estimatedMinimumCredits":1000,
   "estimatedOneSeatCreditsUsed":1000,
   "estimatedOneSeatCashToBePaid":1000,
   "estimatedTwoSeatCreditsUsed":1000,
   "estimatedTwoSeatCashToBePaid":1000,
   "estimatedThreeSeatCreditsUsed":1000,
   "estimatedThreeSeatCashToBePaid":1000
}


//For others
{ 
   "estimatedTotalUserCredits": 2000
   "estimatedCreditsUsed":2000,
   "estimatedCashToBePaid":0,
   "estimatedNegativeCreditsAdjusted":1000,
   "estimatedMinimumCredits":1000
}
```