#10. Driver Credits History

**Url**

```sh
<basePath>/api/transaction/driver/{start}/{length}.json?reportType=1
```

**NOTE**

```
reportType=1 for driver credit top history (moov money top up and admin top up)
reportType=2 for driver credits all history (debit, credit, moov money top, admin top up etc)
```

**Method**

```sh
GET
```

**Headers**

```sh
Content-type: application/json
x-language-code: en
```

**Input Body**

```
No Changes
```

**Output Body**

```
[ 
   { 
      "createdAt":1568655963708,
      "createdBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "updatedAt":1568655963708,
      "updatedBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "recordStatus":"A",
      "driverTaxijetCreditLogId":"479acb95db6a4246bef6c8a6fbe8cc66",
      "driverId":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "originalAmount":404326.0,
      "debitAmount":36000.0,
      "creditAmount":0.0,
      "afterAmount":368326.0,
      "trasactionType":"Driver Payout",
      "tourId":null,
      "status":"success",
      "transactionId":null,
      "driverCheckInOutId":null,
      "comment":"sadasd",
      "driverName":"Driver1 Mobisoft",
      "debit":true,
      "paymentTypeId": "3",
      "paymentType":"Super Admin",
      "orderId":12
   },
   { 
      "createdAt":1568655954497,
      "createdBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "updatedAt":1568655954497,
      "updatedBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "recordStatus":"A",
      "driverTaxijetCreditLogId":"5c4fd6b4d36a4a2f883d89d0fd406da1",
      "driverId":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "originalAmount":354326.0,
      "debitAmount":0.0,
      "creditAmount":50000.0,
      "afterAmount":404326.0,
      "trasactionType":"Admin Top Up",
      "tourId":null,
      "status":"success",
      "transactionId":null,
      "driverCheckInOutId":null,
      "comment":null,
      "driverName":"Driver1 Mobisoft",
      "debit":false,
      "paymentTypeId": "3",
      "paymentType":"Super Admin",
      "orderId":12
   },
   { 
      "createdAt":1568655672653,
      "createdBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "updatedAt":1568655672653,
      "updatedBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "recordStatus":"A",
      "driverTaxijetCreditLogId":"9e8971c7399942bcb7e761d1ef13e011",
      "driverId":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "originalAmount":354500.0,
      "debitAmount":174.0,
      "creditAmount":0.0,
      "afterAmount":354326.0,
      "trasactionType":"Driver Payout",
      "tourId":null,
      "status":"success",
      "transactionId":null,
      "driverCheckInOutId":null,
      "comment":"jhj",
      "driverName":"Driver1 Mobisoft",
      "debit":true,
      "paymentTypeId": "3",
      "paymentType":"Super Admin",
      "orderId":12
   },
   { 
      "createdAt":1568655663000,
      "createdBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "updatedAt":1568655663000,
      "updatedBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "recordStatus":"A",
      "driverTaxijetCreditLogId":"b37d234e847f469486666911999e35bf",
      "driverId":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "originalAmount":356500.0,
      "debitAmount":2000.0,
      "creditAmount":0.0,
      "afterAmount":354500.0,
      "trasactionType":"Driver Payout",
      "tourId":null,
      "status":"success",
      "transactionId":null,
      "driverCheckInOutId":null,
      "comment":"hghfg",
      "driverName":"Driver1 Mobisoft",
      "debit":true,
      "paymentTypeId": "3",
      "paymentType":"Super Admin",
      "orderId":12
   },
   { 
      "createdAt":1568655653004,
      "createdBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "updatedAt":1568655653004,
      "updatedBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "recordStatus":"A",
      "driverTaxijetCreditLogId":"d8100f7579824e6a82ea392437eed7e5",
      "driverId":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "originalAmount":361500.0,
      "debitAmount":5000.0,
      "creditAmount":0.0,
      "afterAmount":356500.0,
      "trasactionType":"Driver Payout",
      "tourId":null,
      "status":"success",
      "transactionId":null,
      "driverCheckInOutId":null,
      "comment":"dfafsd",
      "driverName":"Driver1 Mobisoft",
      "debit":true,
      "paymentTypeId": "3",
      "paymentType":"Super Admin",
      "orderId":12
   },
   { 
      "createdAt":1568655282084,
      "createdBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "updatedAt":1568655282084,
      "updatedBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "recordStatus":"A",
      "driverTaxijetCreditLogId":"248a43f3b46442b6939bd6a2b695e688",
      "driverId":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "originalAmount":311500.0,
      "debitAmount":0.0,
      "creditAmount":50000.0,
      "afterAmount":361500.0,
      "trasactionType":"Admin Top Up",
      "tourId":null,
      "status":"success",
      "transactionId":null,
      "driverCheckInOutId":null,
      "comment":null,
      "driverName":"Driver1 Mobisoft",
      "debit":false,
      "paymentTypeId": "3",
      "paymentType":"Super Admin",
      "orderId":12
   },
   { 
      "createdAt":1568655260097,
      "createdBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "updatedAt":1568655260097,
      "updatedBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "recordStatus":"A",
      "driverTaxijetCreditLogId":"83aada5222d14ef79765cf44cafd4899",
      "driverId":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "originalAmount":111500.0,
      "debitAmount":0.0,
      "creditAmount":200000.0,
      "afterAmount":311500.0,
      "trasactionType":"Admin Top Up",
      "tourId":null,
      "status":"success",
      "transactionId":null,
      "driverCheckInOutId":null,
      "comment":null,
      "driverName":"Driver1 Mobisoft",
      "debit":false,
      "paymentTypeId": "3",
      "paymentType":"Super Admin",
      "orderId":12
   },
   { 
      "createdAt":1568655252144,
      "createdBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "updatedAt":1568655252144,
      "updatedBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "recordStatus":"A",
      "driverTaxijetCreditLogId":"7c3fb9d4444947179c1b39f09ef1140a",
      "driverId":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "originalAmount":11500.0,
      "debitAmount":0.0,
      "creditAmount":100000.0,
      "afterAmount":111500.0,
      "trasactionType":"Admin Top Up",
      "tourId":null,
      "status":"success",
      "transactionId":null,
      "driverCheckInOutId":null,
      "comment":null,
      "driverName":"Driver1 Mobisoft",
      "debit":false,
      "paymentTypeId": "3",
      "paymentType":"Super Admin",
      "orderId":12
   },
   { 
      "createdAt":1568655138825,
      "createdBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "updatedAt":1568655138825,
      "updatedBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "recordStatus":"A",
      "driverTaxijetCreditLogId":"cce13eb563524393b70d21cb0b152423",
      "driverId":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "originalAmount":16500.0,
      "debitAmount":5000.0,
      "creditAmount":0.0,
      "afterAmount":11500.0,
      "trasactionType":"Driver Payout",
      "tourId":null,
      "status":"success",
      "transactionId":null,
      "driverCheckInOutId":null,
      "comment":"SOme payment",
      "driverName":"Driver1 Mobisoft",
      "debit":true,
      "paymentTypeId": "3",
      "paymentType":"Super Admin",
      "orderId":12
   },
   { 
      "createdAt":1568566221314,
      "createdBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "updatedAt":1568566221314,
      "updatedBy":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "recordStatus":"A",
      "driverTaxijetCreditLogId":"3bee0a21be034da18efac596f8bf7818",
      "driverId":"74a2aa2a3d6e4046bb65f2b59441d9ba",
      "originalAmount":17500.0,
      "debitAmount":1000.0,
      "creditAmount":0.0,
      "afterAmount":16500.0,
      "trasactionType":"Driver Payout",
      "tourId":null,
      "status":"success",
      "transactionId":null,
      "driverCheckInOutId":null,
      "comment":"Payout for August",
      "driverName":"Driver1 Mobisoft",
      "debit":true,
      "paymentTypeId": "3",
      "paymentType":"Super Admin",
      "orderId":12
   }
]
```