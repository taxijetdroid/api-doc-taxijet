#16. Driver Top Up -> MTN Transaction Check

**Url**

```sh
<basePath>/api/mtn/{transactionId}.json
```

**Method**

```sh
GET
```

**Headers**

```sh
Content-type: application/json
x-language-code: en
```

**Input Body**

```
No Changes
```

**Output Body**

```
No Changes
```