#44. Cancel Subscription Charges

**Url**

```sh
<basePath>/api/tours/cancellation-charges/v2.json
```

**Note**

```
This is an existing api.
```

**Method**

```sh
GET
```

**Headers**

```sh
Content-type: application/json
x-language-code: en
```

**Input Body**

```
-
```

**Output Body**

```json
{
   "cancelTime":0,
   "charges":0,
   "message":"This trip will be marked as completed. Refund and cancellation charges not applicable."
}
```